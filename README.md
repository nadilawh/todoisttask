# Mobile Automation task


1. Part 1: Automation call API to create test project
2. Part 2: Automate to start the mobile application and verify the project is created in the mobile app
3. Part 3: Automate to create a task in the mobile application and call API to verify task created correctly


## Pre-Req


Katalon,
Appium,
Make sure katalon set up to test android device



```python


```


## Steps


1. Clone This Repository
2. Open project with katalon
3. Set profile to todoist
4. Open Test Cases Folder and Double Click 'SleekFlow Task'
5.  Run with android
=