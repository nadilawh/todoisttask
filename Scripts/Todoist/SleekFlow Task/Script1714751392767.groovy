import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WS.sendRequestAndVerify(findTestObject('Todoist/Add Project'))

Mobile.startApplication('File App/todoist-v11416.apk', false)

// Verify if the 'Button Browse' exists
boolean doesBrowseButtonExist = Mobile.verifyElementExist(findTestObject('Todoist/Button Browse'), 0)

if (!(doesBrowseButtonExist)) {
    // If the 'Button Browse' does not exist, log "Login needed" and delay
    System.out.println('Login needed. Please check if user is logged in.')

    Mobile.delay(90, FailureHandling.STOP_ON_FAILURE // If the 'Button Browse' exists, perform the tap operation
        ) // Verify the text of the 'Name Project' element
} else {
    Mobile.tap(findTestObject('Todoist/Button Browse'), 0)

    Mobile.verifyElementText(findTestObject('Todoist/Name Project'), GlobalVariable.projectName)
}

Mobile.tap(findTestObject('Object Repository/Todoist/btnQuickAddTask'), 0)

Mobile.sendKeys(findTestObject('Object Repository/Todoist/txtMessage'), GlobalVariable.taskName)

Mobile.sendKeys(findTestObject('Object Repository/Todoist/txtDesc'), 'ini ' + GlobalVariable.taskName)

Mobile.tap(findTestObject('Object Repository/Todoist/btnAddTask'), 0)

Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

WS.sendRequestAndVerify(findTestObject('Todoist/Get Task'))

