<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Get Task</name>
   <tag></tag>
   <elementGuidId>380a5886-0d17-4d02-92dc-b3a629352217</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <authorizationRequest>
      <authorizationInfo>
         <entry>
            <key>bearerToken</key>
            <value>${GlobalVariable.token}</value>
         </entry>
      </authorizationInfo>
      <authorizationType>Bearer</authorizationType>
   </authorizationRequest>
   <autoUpdateContent>true</autoUpdateContent>
   <connectionTimeout>0</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${GlobalVariable.token}</value>
      <webElementGuid>777066f0-9cdd-4589-98a2-6f1f83c87881</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>9.4.0</katalonVersion>
   <maxResponseSize>0</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <path></path>
   <restRequestMethod>GET</restRequestMethod>
   <restUrl>https://api.todoist.com/rest/v2/tasks</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>0</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <verificationScript>
import static org.assertj.core.api.Assertions.*
import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager
import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.util.KeywordUtil



RequestObject request = WSResponseManager.getInstance().getCurrentRequest()
ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
def jsonResponse = new JsonSlurper().parseText(response.getResponseBodyContent())

Thread.sleep(5000)
// Inisialisasi variabel untuk mengecek apakah task ditemukan
boolean taskFound = false

// Mengiterasi melalui array JSON dan memverifikasi properti 'content'
jsonResponse.each { item ->
    if (item.content == GlobalVariable.taskName) {
        int index = jsonResponse.indexOf(item)
        WS.verifyElementPropertyValue(response, &quot;[$index].content&quot;, GlobalVariable.taskName)
        WS.comment('FOUND Task: ' + GlobalVariable.taskName)
        taskFound = true
    }
}

// Jika task tidak ditemukan, log pesan kegagalan
if (!taskFound) {
    KeywordUtil.markFailed('Result FAILED: Task with name ' + GlobalVariable.taskName + ' not found in the response.')
}</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
